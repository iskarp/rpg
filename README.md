# Project Title

The idea of project was to make an own collection of RPG characthers. 

## Getting Started

Take a git clone on project and take it to your computer. Run on command line in the project folder "g++ Char.cpp -o main" and then when it should compile run ./main in the same folder. Program starts.
### Prerequisites

You need a windows or linux computer, in which you can build the project. G++ compiler is also needed.

## Documentation
![loose class diagram](/RPG.jpg)


# About the project

This was made by Ilona Skarp for Noroff and Experis Academy Finland.
