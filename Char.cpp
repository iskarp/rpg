#include <iostream>
#include <string>
#include<vector>
class Equipment {
    public:
        int UseEquipment();

};
class Accessories : Equipment{
    protected:
        int power; 
        std::string name;
     public:
        Accessories(int power, std::string name) {
           this->power = power;
        }  
        int UseEquipment(){
            std::cout << name + " in use!!";
            return power;
        } 
         void setState(int state) {
            this->power = state;
        }
        int giveState() {
            return this->power;
        }


};
class Armour : Equipment{

     protected:
        int power; 
        std::string name;
     public:
        Armour(int power, std::string name) {
           this->power = power;
        }    
        int UseEquipment(){
            std::cout << name + " in use!!";
            return power;
        }
         void setState(int state) {
            this->power = state;
        }
        int giveState() {
            return this->power;
        }

};
class Weapon : Equipment {

    protected:
        int power; 
        std::string name;
    public:
       Weapon(int power, std::string name) {
           this->power = power;
           this->name = name;
       }  
        int  UseEquipment(){
            std::cout << "A " +name + " is in use!!";
            return power;
        }
        void setState(int state) {
            this->power = state;
        }
        int giveState() {
            return this->power;
        }

};
class Char {

    protected:
        std::string name;
        int  body;
        int  speed;
        int  knowledge;
    public:
        Char(std::string name, int body, int speed, int knowledge){
            this->name = name;
            this->body = body;
            this->speed = speed;
            this->knowledge = knowledge;
        }
        void changeBody(int body){
            this->body =body;
        }
        void changeSpeed(int newspeed){
            this->speed = newspeed;
        }
        void changeKnoweledge(int newknowledge){
            this->knowledge = newknowledge;
        }
        int giveBody(){
            return this->body;
        }
        int giveSpeed(){
            return this->speed;
        }
        int giveKnoweledge(){
            return this-> knowledge;
        }
        void  Greet(){
            std::cout << "Here is a super char " << this->name;;
        }

};

class Warrior : Char {

    protected:
        int power;
        std::string name;
        int body;
        int speed;
        int knoweledge;
        std::vector<Weapon> weapons;
        
    public:
        Warrior(std::string name,int power, int body,int speed,int knoweledge) 
        : Char(name, body, speed, knoweledge) {
            this->power = power; 
            this->name = name;
            this->body = body;
            this->speed = speed;
            this->knowledge = knowledge;
            weapons.push_back(Weapon(100,"sword"));
        }
        void changeBody(int body){
            this->body =body;
        }
        void changeSpeed(int newspeed){
            this->speed = newspeed;
        }
        void changeKnoweledge(int newknowledge){
            this->knowledge = newknowledge;
        }
        int giveBody(){
            return this->body;
        }
        int giveSpeed(){
            return this->speed;
        }
        int giveKnoweledge(){
            return this-> knowledge;
        }
        void Greet(){
            std::cout << "Here is a super warrior " << this->name;;
        }
        void setWeapon(std::string name, int power) {
            Weapon w = Weapon(power, name);
            weapons.push_back(w);
        }
        Weapon giveWeapon(int i) {
            return weapons[i];
        }

};
class Mage : Char {

    protected:
        int power;
        std::string name;
        int body;
        int speed;
        int knoweledge;
        std::vector<Accessories> accessories;
        
    public:
        Mage(std::string name,int power, int body,int speed,int knoweledge) 
        : Char(name, body, speed, knoweledge) {
            this->power = power; 
            this->name = name;
            this->body = body;
            this->speed = speed;
            this->knowledge = knowledge;
            this->accessories.push_back(Accessories(100,"magical book"));

        }
        void changeBody(int body){
            this->body =body;
        }
        void changeSpeed(int newspeed){
            this->speed = newspeed;
        }
        void changeKnoweledge(int newknowledge){
            this->knowledge = newknowledge;
        }
        int giveBody(){
            return this->body;
        }
        int giveSpeed(){
            return this->speed;
        }
        int giveKnoweledge(){
            return this-> knowledge;
        }
        void Greet(){
            std::cout << "Here is a super mage " << this->name;;
        }
       void setAccessories(std::string name, int power) {
            Accessories a= Accessories(power, name);
            this->accessories.push_back(a);
        }
        Accessories giveAccessories(int i) {
            return this->accessories.at(i);
        }

};
class Rogue : Char {

    protected:
        int power;
        std::string name;
        int body;
        int speed;
        int knoweledge;
        std::vector<Armour> armours;
        
        
    public:
        Rogue(std::string name,int power, int body,int speed,int knoweledge) 
        : Char(name, body, speed, knoweledge) {
            this->power = power; 
            this->name = name;
            this->body = body;
            this->speed = speed;
            this->knowledge = knowledge;
            this->armours.push_back(Armour(100,"steel armour"));
        }
        void changeBody(int body){
            this->body =body;
        }
        void changeSpeed(int newspeed){
            this->speed = newspeed;
        }
        void changeKnoweledge(int newknowledge){
            this->knowledge = newknowledge;
        }
        int giveBody(){
            return this->body;
        }
        int giveSpeed(){
            return this->speed;
        }
        int giveKnoweledge(){
            return this-> knowledge;
        }
        void Greet(){
            std::cout << "Here is a super rogue " << this->name;;
        }
       void setArmours(std::string name, int power) {
            Armour a= Armour(power, name);
            armours.push_back(a);
        }
        Armour giveArmour(int i) {
            return this->armours[i];
        }

};

class Ranger : Char {

    protected:
        int power;
        std::string name;
        int body;
        int speed;
        int knoweledge;
        std::vector<Accessories> accessories;
        
    public:
        Ranger(std::string name,int power, int body,int speed,int knoweledge) 
        : Char(name, body, speed, knoweledge) {
            this->power = power; 
            this->name = name;
            this->body = body;
            this->speed = speed;
            this->knowledge = knowledge;
            accessories.push_back(Accessories(100,"magical cloack"));
        }
        void changeBody(int body){
            this->body =body;
        }
        void changeSpeed(int newspeed){
            this->speed = newspeed;
        }
        void changeKnoweledge(int newknowledge){
            this->knowledge = newknowledge;
        }
        int giveBody(){
            return this->body;
        }
        int giveSpeed(){
            return this->speed;
        }
        int giveKnoweledge(){
            return this-> knowledge;
        }
        void Greet(){
            std::cout << "Here is a super ranger " << this->name;;
        }
        void setAccessories(std::string name, int power) {
            Accessories a= Accessories(power, name);
            this->accessories.push_back(a);
        }
        Accessories giveAccessories(int i) {
            return this->accessories.at(i);
        }
      

};

int main() {


    Warrior me = Warrior("Me",100,100,100,100);
    me.Greet();
    std::cout << me.giveWeapon(0).UseEquipment();
    Mage milla = Mage("Milla", 100,100,100,100);
    milla.Greet();
    Rogue r = Rogue("R", 100,100,100,100);
    r.Greet();
    Ranger he = Ranger("Heman", 100,100,100,100);
    he.Greet();
    return 0;

}

   